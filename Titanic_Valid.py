import tensorflow as tf

import pandas as pd
import numpy as np
t=pd.read_csv("d://train.csv")
#print(t)
t["Age"]=t["Age"].fillna(t["Age"].median())
# print(t.describe())
t.loc[t["Sex"]=="male","Sex"]=0
t.loc[t["Sex"]=="female","Sex"]=1
t["Embarked"]=t["Embarked"].fillna("S")
t.loc[t["Embarked"]=="S","Embarked"]=0
t.loc[t["Embarked"]=="C","Embarked"]=1
t.loc[t["Embarked"]=="Q","Embarked"]=2
# print(t["Embarked"].unique())

import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import KFold
predictors = ["Pclass", "Sex", "Age", "SibSp","Parch","Fare","Embarked"]
alg=LinearRegression()
kf=KFold(n_splits=3,random_state=1)
predictions=[]
print("------------------------------------------------------------------------")
# print(kf.split(t.shape[0]))
# X=np.array([[1,2],[3,4],[5,6],[7,8],[9,10],[11,12]])
# kf2=KFold(n_splits=2)    # 定义分成几个组
# kf.get_n_splits(X)    # 查询分成几个组

# print(X)
# print(type(X))
# print(type(t.shape[0]))
# print(t.shape[0])
# print(t)
# print(kf2)
# for train_index,test_index in kf2.split(X):
#     print("Train Index:",train_index,",Test Index:",test_index)
#     X_train,X_test=X[train_index],X[test_index]
# print(t.shape[0])
print("------------------------------------------------------------------------")
for train,test in kf.split(t):
    train_predictors=(t[predictors].iloc[train, :])
    train_target=t["Survived"].iloc[train]
    alg.fit(train_predictors,train_target)
    test_predictions=alg.predict(t[predictors].iloc[test, :])
    predictions.append(test_predictions)

predictions=np.concatenate(predictions,axis=0)
predictions[predictions >.5]=1
predictions[predictions <=.5]=0
predictions.dtype = "float64"
t["Survived"] = t["Survived"].astype(float)
print("Test total count: ", len(predictions))
# print(predictions)
print("Correct count: ", sum(predictions == t["Survived"]))
print("Accuracy: ", sum(predictions == t["Survived"]) / len(predictions))

from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
alg = LogisticRegression(random_state = 1)
score = cross_val_score(alg, t[predictors], t["Survived"], cv = 3)
# print(score.mean())

from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
predictors = ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked"]
alg = RandomForestClassifier(random_state=1, n_estimators = 100, min_samples_split = 4, min_samples_leaf=2)
kf=KFold(n_splits=3, random_state=1)
scores=cross_val_score(alg, t[predictors], t["Survived"], cv=kf)
print("mean of RandomForestClassifier: "
      ,scores.mean())

# new attributes
t["FamilySize"] = t["SibSp"] + t["Parch"]
t["NameLength"] = t["Name"].apply(lambda x:len(x))
# print(t)

import re
import pandas as pd


def get_title(name):
    title_search = re.search(' ([A-Za-z]+)\.', name)
    if title_search:
        return title_search.group(1)
    return ""


titles = t["Name"].apply(get_title)
print(pd.value_counts(titles))
print("1---------------------------------------------------------------------------------")
print("2---------------------------------------------------------------------------------")
title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Dr": 5, "Rev": 6, "Major": 7, "Col": 7, "Mlle": 8,
                 "Mme": 8, "Don": 9, "Lady": 10, "Countess": 10, "Jonkheer": 10, "Sir": 9, "Capt": 7, "Ms": 2}
for k, v in title_mapping.items():
    titles[titles == k] = v
print("3---------------------------------------------------------------------------------")
print(pd.value_counts(titles))
print("4---------------------------------------------------------------------------------")
t["Title"] = titles

import numpy as np
from sklearn.feature_selection import SelectKBest, f_classif
import matplotlib.pyplot as plt
predictors = ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked", "FamilySize", "Title", "NameLength"]
selector = SelectKBest(f_classif, k=5)
selector.fit(t[predictors], t["Survived"])
print("5---------------------------------------------------------------------------------")
print(range(len(predictors)))
print(scores)
scores = -np.log10(selector.pvalues_)
print(scores)
plt.bar(range(len(predictors)), scores)
print("6---------------------------------------------------------------------------------")

plt.xticks(range(len(predictors)), predictors, rotation="vertical")
plt.show()
predictors = ["Pclass", "Sex", "Fare", "Title"]
print("7---------------------------------------------------------------------------------")

alg = RandomForestClassifier(random_state = 1, n_estimators = 50, min_samples_split = 8, min_samples_leaf = 4)
kf=KFold(n_splits = 3, random_state = 1)
scores = cross_val_score(alg, t[predictors], t["Survived"], cv = kf)
print(scores.mean())

from sklearn.ensemble import GradientBoostingClassifier
import numpy as np

algorithm = [
    [GradientBoostingClassifier(random_state = 1, n_estimators = 50, max_depth = 6),
     ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked", "FamilySize", "NameLength", "Title"]],
    [LogisticRegression(random_state = 1),
     ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked", "FamilySize", "NameLength", "Title"]]
]

kf=KFold(n_splits = 3, random_state = 1)
predictions = []
for train, test in kf.split(t):
    train_target = t["Survived"].iloc[train]
    full_test_predictions = []
    for alg, predictors in algorithm:
        alg.fit(t[predictors].iloc[train,:], train_target)
        test_predictions = alg.predict_proba(t[predictors].iloc[test,:].astype(float))[:,1]
        full_test_predictions.append(test_predictions)
    test_predictions = (full_test_predictions[0] + full_test_predictions[1]) / 2
    test_predictions[test_predictions > .5] = 1
    test_predictions[test_predictions <= .5] = 0
    predictions.append(test_predictions)
predictions = np.concatenate(predictions, axis = 0)
accury = sum(predictions == t["Survived"]) / len(predictions)
print(accury)
