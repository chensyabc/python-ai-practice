import tensorflow as tf
import numpy as np

x_data = np.random.rand(100).astype("float32")

with tf.name_scope('y_data'):
    y_data = x_data * 2.5 + 0.8
    tf.summary.histogram("method_demo" + "/y_data", y_data)

with tf.name_scope("W"):
    W = tf.Variable(tf.random_uniform([1], -200.0, 200.0))
    tf.summary.histogram("method_demo" + "/W", W)

with tf.name_scope("b"):
    b = tf.Variable(tf.zeros([1]))
    tf.summary.histogram("method_demo" + "/b", b)

with tf.name_scope("y"):
    y = W * x_data + b
    tf.summary.histogram("method_demo" + '/y', y)

with tf.name_scope("loss"):
    loss = tf.reduce_mean(tf.square(y - y_data))
    tf.summary.histogram("method_demo" + "/loss", loss)
    tf.summary.scalar("method_demo" + "loss", loss)

optimizer = tf.train.GradientDescentOptimizer(0.7)
with tf.name_scope('train'):
    train = optimizer.minimize(loss)

init = tf.global_variables_initializer()

sess = tf.Session()

merged = tf.summary.merge_all()

writer = tf.summary.FileWriter("D://", sess.graph)

sess.run(init)

for step in range(500):
    sess.run(train)
    if step % 5 == 0:
        print(step, "W:", sess.run(W), "b:", sess.run(b))
        result = sess.run(merged)
        writer.add_summary(result, step)
